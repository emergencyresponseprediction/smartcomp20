%%% -*-BibTeX-*-
%%% Do NOT edit. File created by BibTeX with style
%%% ACM-Reference-Format-Journals [18-Jan-2012].

\begin{thebibliography}{36}

%%% ====================================================================
%%% NOTE TO THE USER: you can override these defaults by providing
%%% customized versions of any of these macros before the \bibliography
%%% command.  Each of them MUST provide its own final punctuation,
%%% except for \shownote{}, \showDOI{}, and \showURL{}.  The latter two
%%% do not use final punctuation, in order to avoid confusing it with
%%% the Web address.
%%%
%%% To suppress output of a particular field, define its macro to expand
%%% to an empty string, or better, \unskip, like this:
%%%
%%% \newcommand{\showDOI}[1]{\unskip}   % LaTeX syntax
%%%
%%% \def \showDOI #1{\unskip}           % plain TeX syntax
%%%
%%% ====================================================================

\ifx \showCODEN    \undefined \def \showCODEN     #1{\unskip}     \fi
\ifx \showDOI      \undefined \def \showDOI       #1{#1}\fi
\ifx \showISBNx    \undefined \def \showISBNx     #1{\unskip}     \fi
\ifx \showISBNxiii \undefined \def \showISBNxiii  #1{\unskip}     \fi
\ifx \showISSN     \undefined \def \showISSN      #1{\unskip}     \fi
\ifx \showLCCN     \undefined \def \showLCCN      #1{\unskip}     \fi
\ifx \shownote     \undefined \def \shownote      #1{#1}          \fi
\ifx \showarticletitle \undefined \def \showarticletitle #1{#1}   \fi
\ifx \showURL      \undefined \def \showURL       {\relax}        \fi
% The following commands are used for tagged output and should be
% invisible to TeX
\providecommand\bibfield[2]{#2}
\providecommand\bibinfo[2]{#2}
\providecommand\natexlab[1]{#1}
\providecommand\showeprint[2][]{arXiv:#2}

\bibitem[\protect\citeauthoryear{??}{cli}{2014}]%
        {climate2014}
 \bibinfo{year}{2014}\natexlab{}.
\newblock \bibinfo{title}{Executive Summary of the National Climate
  Assessment}.
\newblock
\newblock
\urldef\tempurl%
\url{https://nca2014.globalchange.gov/highlights/report-findings/extreme-weather}
\showURL{%
\tempurl}


\bibitem[\protect\citeauthoryear{??}{cal}{2019}]%
        {californiadrought}
 \bibinfo{year}{2019}\natexlab{}.
\newblock \bibinfo{title}{Drought.gov,US Drought Portal}.
\newblock
\newblock
\urldef\tempurl%
\url{https://www.drought.gov/drought/states/california}
\showURL{%
\tempurl}


\bibitem[\protect\citeauthoryear{??}{cod}{2019}]%
        {codepaper}
 \bibinfo{year}{2019}\natexlab{}.
\newblock \bibinfo{title}{Paper: Code and Data}.
\newblock
\newblock
\urldef\tempurl%
\url{https://bitbucket.org/gissemari/water-consumption-prediction}
\showURL{%
\tempurl}


\bibitem[\protect\citeauthoryear{??}{cap}{2019}]%
        {capetownwater}
 \bibinfo{year}{2019}\natexlab{}.
\newblock \bibinfo{title}{Wikipedia: Cape Town Water Crisis}.
\newblock
\newblock
\urldef\tempurl%
\url{https://en.wikipedia.org/wiki/Cape_Town_water_crisis}
\showURL{%
\tempurl}


\bibitem[\protect\citeauthoryear{Ali}{Ali}{2018}]%
        {ALI2018104}
\bibfield{author}{\bibinfo{person}{Babkir Ali}.}
  \bibinfo{year}{2018}\natexlab{}.
\newblock \showarticletitle{Forecasting model for water-energy nexus in
  Alberta, Canada}.
\newblock \bibinfo{journal}{\emph{Water-Energy Nexus}} \bibinfo{volume}{1},
  \bibinfo{number}{2} (\bibinfo{year}{2018}), \bibinfo{pages}{104 -- 115}.
\newblock
\showISSN{2588-9125}
\urldef\tempurl%
\url{https://doi.org/10.1016/j.wen.2018.08.002}
\showDOI{\tempurl}


\bibitem[\protect\citeauthoryear{Arjunan, Srivastava, Singh, and Singh}{Arjunan
  et~al\mbox{.}}{2015}]%
        {arjunan2015openban}
\bibfield{author}{\bibinfo{person}{Pandarasamy Arjunan}, \bibinfo{person}{Mani
  Srivastava}, \bibinfo{person}{Amarjeet Singh}, {and}
  \bibinfo{person}{Pushpendra Singh}.} \bibinfo{year}{2015}\natexlab{}.
\newblock \showarticletitle{OpenBAN: An Open Building ANalytics Middleware for
  Smart Buildings}. In \bibinfo{booktitle}{\emph{proceedings of the 12th EAI
  International Conference on Mobile and Ubiquitous Systems: Computing,
  Networking and Services}}. \bibinfo{pages}{70--79}.
\newblock


\bibitem[\protect\citeauthoryear{Assem, Ghariba, Makrai, Johnston, Gill, and
  Pilla}{Assem et~al\mbox{.}}{2017}]%
        {assem2017urban}
\bibfield{author}{\bibinfo{person}{Haytham Assem}, \bibinfo{person}{Salem
  Ghariba}, \bibinfo{person}{Gabor Makrai}, \bibinfo{person}{Paul Johnston},
  \bibinfo{person}{Laurence Gill}, {and} \bibinfo{person}{Francesco Pilla}.}
  \bibinfo{year}{2017}\natexlab{}.
\newblock \showarticletitle{Urban water flow and water level prediction based
  on deep learning}. In \bibinfo{booktitle}{\emph{Joint European Conference on
  Machine Learning and Knowledge Discovery in Databases}}. Springer,
  \bibinfo{pages}{317--329}.
\newblock


\bibitem[\protect\citeauthoryear{Bejarano, DeFazio, and Ramesh}{Bejarano
  et~al\mbox{.}}{2019}]%
        {bejarano2019}
\bibfield{author}{\bibinfo{person}{Gissella Bejarano}, \bibinfo{person}{David
  DeFazio}, {and} \bibinfo{person}{Arti Ramesh}.}
  \bibinfo{year}{2019}\natexlab{}.
\newblock \showarticletitle{Deep Latent Generative Models For Energy
  Disaggregation}. In \bibinfo{booktitle}{\emph{Thirty-Third AAAI Conference on
  Artificial Intelligence}}.
\newblock


\bibitem[\protect\citeauthoryear{Bejarano, Jain, Ramesh, Seetharam, and
  Mishra}{Bejarano et~al\mbox{.}}{2018}]%
        {bejarano2018predictive}
\bibfield{author}{\bibinfo{person}{Gissella Bejarano}, \bibinfo{person}{Mayank
  Jain}, \bibinfo{person}{Arti Ramesh}, \bibinfo{person}{Anand Seetharam},
  {and} \bibinfo{person}{Aditya Mishra}.} \bibinfo{year}{2018}\natexlab{}.
\newblock \showarticletitle{Predictive analytics for smart water management in
  developing regions}. In \bibinfo{booktitle}{\emph{2018 IEEE International
  Conference on Smart Computing (SMARTCOMP)}}. IEEE, \bibinfo{pages}{464--469}.
\newblock


\bibitem[\protect\citeauthoryear{Bhandari, Nambi, Padmanabhan, and
  Raman}{Bhandari et~al\mbox{.}}{2018}]%
        {deeplane}
\bibfield{author}{\bibinfo{person}{Ravi Bhandari}, \bibinfo{person}{Akshay
  Nambi}, \bibinfo{person}{Venkat Padmanabhan}, {and}
  \bibinfo{person}{Bhaskaran Raman}.} \bibinfo{year}{2018}\natexlab{}.
\newblock \showarticletitle{DeepLane: camera-assisted GPS for driving lane
  detection}. In \bibinfo{booktitle}{\emph{Proceedings of the 5th Conference on
  Systems for Built Environments}}. ACM.
\newblock


\bibitem[\protect\citeauthoryear{Bhardwaj, Tummala, Ramalingam, Ramjee, and
  Sinha}{Bhardwaj et~al\mbox{.}}{2017}]%
        {bhardwaj2017autocalib}
\bibfield{author}{\bibinfo{person}{Romil Bhardwaj},
  \bibinfo{person}{Gopi~Krishna Tummala}, \bibinfo{person}{Ganesan Ramalingam},
  \bibinfo{person}{Ramachandran Ramjee}, {and} \bibinfo{person}{Prasun Sinha}.}
  \bibinfo{year}{2017}\natexlab{}.
\newblock \showarticletitle{Autocalib: automatic traffic camera calibration at
  scale}. In \bibinfo{booktitle}{\emph{Proceedings of the 4th ACM International
  Conference on Systems for Energy-Efficient Built Environments}}. ACM,
  \bibinfo{pages}{14}.
\newblock


\bibitem[\protect\citeauthoryear{Borowik, Wawrzyniak, and Cichosz}{Borowik
  et~al\mbox{.}}{2018}]%
        {borowik2018time}
\bibfield{author}{\bibinfo{person}{Grzegorz Borowik},
  \bibinfo{person}{Zbigniew~M Wawrzyniak}, {and} \bibinfo{person}{Pawe{\l}
  Cichosz}.} \bibinfo{year}{2018}\natexlab{}.
\newblock \showarticletitle{Time series analysis for crime forecasting}. In
  \bibinfo{booktitle}{\emph{2018 26th International Conference on Systems
  Engineering (ICSEng)}}. IEEE, \bibinfo{pages}{1--10}.
\newblock


\bibitem[\protect\citeauthoryear{Candelieri, Soldi, and Archetti}{Candelieri
  et~al\mbox{.}}{2015}]%
        {CANDELIERI2015844}
\bibfield{author}{\bibinfo{person}{Antonio Candelieri}, \bibinfo{person}{Davide
  Soldi}, {and} \bibinfo{person}{Francesco Archetti}.}
  \bibinfo{year}{2015}\natexlab{}.
\newblock \showarticletitle{Short-term forecasting of hourly water consumption
  by using automatic metering readers data}.
\newblock \bibinfo{journal}{\emph{Procedia Engineering}}  \bibinfo{volume}{119}
  (\bibinfo{year}{2015}), \bibinfo{pages}{844 -- 853}.
\newblock
\showISSN{1877-7058}
\urldef\tempurl%
\url{https://doi.org/10.1016/j.proeng.2015.08.948}
\showDOI{\tempurl}
\newblock
\shownote{Computing and Control for the Water Industry (CCWI2015) Sharing the
  best practice in water management.}


\bibitem[\protect\citeauthoryear{Chen, Breda, and Irwin}{Chen
  et~al\mbox{.}}{2018}]%
        {chen2018staring}
\bibfield{author}{\bibinfo{person}{Dong Chen}, \bibinfo{person}{Joseph Breda},
  {and} \bibinfo{person}{David Irwin}.} \bibinfo{year}{2018}\natexlab{}.
\newblock \showarticletitle{Staring at the sun: a physical black-box solar
  performance model}. In \bibinfo{booktitle}{\emph{Proceedings of the 5th
  Conference on Systems for Built Environments}}. ACM, \bibinfo{pages}{53--62}.
\newblock


\bibitem[\protect\citeauthoryear{Cho, Gulcehre, Bahdanau, Schwenk, and
  Bengio}{Cho et~al\mbox{.}}{[n.d.]}]%
        {cholearning}
\bibfield{author}{\bibinfo{person}{Kyunghyun Cho}, \bibinfo{person}{Bart van
  Merri{\"e}nboer~Caglar Gulcehre}, \bibinfo{person}{Dzmitry Bahdanau},
  \bibinfo{person}{Fethi Bougares~Holger Schwenk}, {and}
  \bibinfo{person}{Yoshua Bengio}.} \bibinfo{year}{[n.d.]}\natexlab{}.
\newblock \showarticletitle{Learning Phrase Representations using RNN
  Encoder--Decoder for Statistical Machine Translation}.
\newblock  (\bibinfo{year}{[n.\,d.]}).
\newblock


\bibitem[\protect\citeauthoryear{DeFazio, Ramesh, and Seetharam}{DeFazio
  et~al\mbox{.}}{2018}]%
        {defazio2018nycer}
\bibfield{author}{\bibinfo{person}{David DeFazio}, \bibinfo{person}{Arti
  Ramesh}, {and} \bibinfo{person}{Anand Seetharam}.}
  \bibinfo{year}{2018}\natexlab{}.
\newblock \showarticletitle{NYCER: A Non-Emergency Response Predictor for NYC
  using Sparse Gaussian Conditional Random Fields}. In
  \bibinfo{booktitle}{\emph{Proceedings of the 15th EAI International
  Conference on Mobile and Ubiquitous Systems: Computing, Networking and
  Services}}. ACM, \bibinfo{pages}{187--196}.
\newblock


\bibitem[\protect\citeauthoryear{Endo, Tsurita, Burnett, and Orencio}{Endo
  et~al\mbox{.}}{2017}]%
        {endo2017review}
\bibfield{author}{\bibinfo{person}{Aiko Endo}, \bibinfo{person}{Izumi Tsurita},
  \bibinfo{person}{Kimberly Burnett}, {and} \bibinfo{person}{Pedcris~M
  Orencio}.} \bibinfo{year}{2017}\natexlab{}.
\newblock \showarticletitle{A review of the current state of research on the
  water, energy, and food nexus}.
\newblock \bibinfo{journal}{\emph{Journal of Hydrology: Regional Studies}}
  \bibinfo{volume}{11} (\bibinfo{year}{2017}), \bibinfo{pages}{20--30}.
\newblock


\bibitem[\protect\citeauthoryear{Fisher, Shields, Chan, Christenson, Cronk,
  Leker, Samani, Apoya, Lutz, and Bartram}{Fisher et~al\mbox{.}}{2015}]%
        {fisher2015understanding}
\bibfield{author}{\bibinfo{person}{Michael~B Fisher},
  \bibinfo{person}{Katherine~F Shields}, \bibinfo{person}{Terence~U Chan},
  \bibinfo{person}{Elizabeth Christenson}, \bibinfo{person}{Ryan~D Cronk},
  \bibinfo{person}{Hannah Leker}, \bibinfo{person}{Destina Samani},
  \bibinfo{person}{Patrick Apoya}, \bibinfo{person}{Alexandra Lutz}, {and}
  \bibinfo{person}{Jamie Bartram}.} \bibinfo{year}{2015}\natexlab{}.
\newblock \showarticletitle{Understanding handpump sustainability: Determinants
  of rural water source functionality in the Greater Afram Plains region of
  Ghana}.
\newblock \bibinfo{journal}{\emph{Water resources research}}
  \bibinfo{volume}{51}, \bibinfo{number}{10} (\bibinfo{year}{2015}),
  \bibinfo{pages}{8431--8449}.
\newblock


\bibitem[\protect\citeauthoryear{Foster}{Foster}{2013}]%
        {foster2013predictors}
\bibfield{author}{\bibinfo{person}{Tim Foster}.}
  \bibinfo{year}{2013}\natexlab{}.
\newblock \showarticletitle{Predictors of sustainability for community-managed
  handpumps in sub-Saharan Africa: evidence from Liberia, Sierra Leone, and
  Uganda}.
\newblock \bibinfo{journal}{\emph{Environmental science \& technology}}
  \bibinfo{volume}{47}, \bibinfo{number}{21} (\bibinfo{year}{2013}),
  \bibinfo{pages}{12037--12046}.
\newblock


\bibitem[\protect\citeauthoryear{Goodfellow, Bengio, and Courville}{Goodfellow
  et~al\mbox{.}}{2016}]%
        {goodfellow2016deep}
\bibfield{author}{\bibinfo{person}{Ian Goodfellow}, \bibinfo{person}{Yoshua
  Bengio}, {and} \bibinfo{person}{Aaron Courville}.}
  \bibinfo{year}{2016}\natexlab{}.
\newblock \bibinfo{booktitle}{\emph{Deep learning}}.
\newblock


\bibitem[\protect\citeauthoryear{Hochreiter}{Hochreiter}{1998}]%
        {hochreiter1998vanishing}
\bibfield{author}{\bibinfo{person}{Sepp Hochreiter}.}
  \bibinfo{year}{1998}\natexlab{}.
\newblock \showarticletitle{The vanishing gradient problem during learning
  recurrent neural nets and problem solutions}.
\newblock \bibinfo{journal}{\emph{International Journal of Uncertainty,
  Fuzziness and Knowledge-Based Systems}} \bibinfo{volume}{6},
  \bibinfo{number}{02} (\bibinfo{year}{1998}), \bibinfo{pages}{107--116}.
\newblock


\bibitem[\protect\citeauthoryear{Hoff}{Hoff}{2011}]%
        {hoff2011understanding}
\bibfield{author}{\bibinfo{person}{H Hoff}.} \bibinfo{year}{2011}\natexlab{}.
\newblock \bibinfo{title}{Understanding the Nexus; Background paper for the
  Bonn2011 Conference: The Water, Energy and Food Security Nexus; Stockholm
  Environment Institute: Stockholm, Sweden, 2011}.
\newblock
\newblock


\bibitem[\protect\citeauthoryear{Jabbari and Bae}{Jabbari and Bae}{2018}]%
        {jabbari2018application}
\bibfield{author}{\bibinfo{person}{Aida Jabbari} {and} \bibinfo{person}{Deg-Hyo
  Bae}.} \bibinfo{year}{2018}\natexlab{}.
\newblock \showarticletitle{Application of Artificial Neural Networks for
  Accuracy Enhancements of Real-Time Flood Forecasting in the Imjin Basin}.
\newblock \bibinfo{journal}{\emph{Water}} \bibinfo{volume}{10},
  \bibinfo{number}{11} (\bibinfo{year}{2018}), \bibinfo{pages}{1626}.
\newblock


\bibitem[\protect\citeauthoryear{Kang and Kang}{Kang and Kang}{2017}]%
        {kang2017prediction}
\bibfield{author}{\bibinfo{person}{Hyeon-Woo Kang} {and}
  \bibinfo{person}{Hang-Bong Kang}.} \bibinfo{year}{2017}\natexlab{}.
\newblock \showarticletitle{Prediction of crime occurrence from multi-modal
  data using deep learning}.
\newblock \bibinfo{journal}{\emph{PloS one}} \bibinfo{volume}{12},
  \bibinfo{number}{4} (\bibinfo{year}{2017}), \bibinfo{pages}{e0176244}.
\newblock


\bibitem[\protect\citeauthoryear{Kofinas, Papageorgiou, Laspidou, Mellios, and
  Kokkinos}{Kofinas et~al\mbox{.}}{2016}]%
        {kofinas2016daily}
\bibfield{author}{\bibinfo{person}{Dimitris Kofinas}, \bibinfo{person}{Elpiniki
  Papageorgiou}, \bibinfo{person}{C Laspidou}, \bibinfo{person}{Nikolaos
  Mellios}, {and} \bibinfo{person}{Konstantinos Kokkinos}.}
  \bibinfo{year}{2016}\natexlab{}.
\newblock \showarticletitle{Daily multivariate forecasting of water demand in a
  touristic island with the use of artificial neural network and adaptive
  neuro-fuzzy inference system}. In \bibinfo{booktitle}{\emph{2016
  International Workshop on Cyber-physical Systems for Smart Water Networks
  (CySWater)}}. IEEE, \bibinfo{pages}{37--42}.
\newblock


\bibitem[\protect\citeauthoryear{Kwon, Fischer, Flintham, and Colley}{Kwon
  et~al\mbox{.}}{2018}]%
        {kwon2018connected}
\bibfield{author}{\bibinfo{person}{Hyosun Kwon}, \bibinfo{person}{Joel~E
  Fischer}, \bibinfo{person}{Martin Flintham}, {and} \bibinfo{person}{James
  Colley}.} \bibinfo{year}{2018}\natexlab{}.
\newblock \showarticletitle{The Connected Shower: Studying Intimate Data in
  Everyday Life}.
\newblock \bibinfo{journal}{\emph{Proceedings of the ACM on Interactive,
  Mobile, Wearable and Ubiquitous Technologies}} \bibinfo{volume}{2},
  \bibinfo{number}{4} (\bibinfo{year}{2018}), \bibinfo{pages}{176}.
\newblock


\bibitem[\protect\citeauthoryear{Lafferty, McCallum, and Pereira}{Lafferty
  et~al\mbox{.}}{2001}]%
        {lafferty2001conditional}
\bibfield{author}{\bibinfo{person}{John Lafferty}, \bibinfo{person}{Andrew
  McCallum}, {and} \bibinfo{person}{Fernando~CN Pereira}.}
  \bibinfo{year}{2001}\natexlab{}.
\newblock \showarticletitle{Conditional random fields: Probabilistic models for
  segmenting and labeling sequence data}.
\newblock  (\bibinfo{year}{2001}).
\newblock


\bibitem[\protect\citeauthoryear{Mason, Duggan, and Howley}{Mason
  et~al\mbox{.}}{2018}]%
        {mason2018forecasting}
\bibfield{author}{\bibinfo{person}{Karl Mason}, \bibinfo{person}{Jim Duggan},
  {and} \bibinfo{person}{Enda Howley}.} \bibinfo{year}{2018}\natexlab{}.
\newblock \showarticletitle{Forecasting energy demand, wind generation and
  carbon dioxide emissions in Ireland using evolutionary neural networks}.
\newblock \bibinfo{journal}{\emph{Energy}}  \bibinfo{volume}{155}
  (\bibinfo{year}{2018}), \bibinfo{pages}{705--720}.
\newblock


\bibitem[\protect\citeauthoryear{Melzi, Touati, Same, and Oukhellou}{Melzi
  et~al\mbox{.}}{2016}]%
        {melzi2016hourly}
\bibfield{author}{\bibinfo{person}{Fateh~Nassim Melzi}, \bibinfo{person}{Taieb
  Touati}, \bibinfo{person}{Allou Same}, {and} \bibinfo{person}{Latifa
  Oukhellou}.} \bibinfo{year}{2016}\natexlab{}.
\newblock \showarticletitle{Hourly solar irradiance forecasting based on
  machine learning models}. In \bibinfo{booktitle}{\emph{2016 15th IEEE
  International Conference on Machine Learning and Applications (ICMLA)}}.
  IEEE, \bibinfo{pages}{441--446}.
\newblock


\bibitem[\protect\citeauthoryear{Mittal, Yagnik, Garg, and Krishnan}{Mittal
  et~al\mbox{.}}{2016}]%
        {mittal2016spotgarbage}
\bibfield{author}{\bibinfo{person}{Gaurav Mittal}, \bibinfo{person}{Kaushal~B
  Yagnik}, \bibinfo{person}{Mohit Garg}, {and} \bibinfo{person}{Narayanan~C
  Krishnan}.} \bibinfo{year}{2016}\natexlab{}.
\newblock \showarticletitle{SpotGarbage: smartphone app to detect garbage using
  deep learning}. In \bibinfo{booktitle}{\emph{Proceedings of the 2016 ACM
  International Joint Conference on Pervasive and Ubiquitous Computing}}. ACM,
  \bibinfo{pages}{940--945}.
\newblock


\bibitem[\protect\citeauthoryear{Rambo, Warsinger, Shanbhogue, V, and
  Ghoniem}{Rambo et~al\mbox{.}}{2017}]%
        {RAMBO20173837}
\bibfield{author}{\bibinfo{person}{Khulood~A. Rambo}, \bibinfo{person}{David~M.
  Warsinger}, \bibinfo{person}{Santosh~J. Shanbhogue}, \bibinfo{person}{John
  H.~Lienhard V}, {and} \bibinfo{person}{Ahmed~F. Ghoniem}.}
  \bibinfo{year}{2017}\natexlab{}.
\newblock \showarticletitle{Water-Energy Nexus in Saudi Arabia}.
\newblock \bibinfo{journal}{\emph{Energy Procedia}}  \bibinfo{volume}{105}
  (\bibinfo{year}{2017}), \bibinfo{pages}{3837 -- 3843}.
\newblock
\showISSN{1876-6102}
\urldef\tempurl%
\url{https://doi.org/10.1016/j.egypro.2017.03.782}
\showDOI{\tempurl}
\newblock
\shownote{8th International Conference on Applied Energy, ICAE2016, 8-11
  October 2016, Beijing, China.}


\bibitem[\protect\citeauthoryear{Tull, Schmitt, and Atwater}{Tull
  et~al\mbox{.}}{[n.d.]}]%
        {tull2016much}
\bibfield{author}{\bibinfo{person}{Christopher Tull}, \bibinfo{person}{Eric
  Schmitt}, {and} \bibinfo{person}{Patrick Atwater}.}
  \bibinfo{year}{[n.d.]}\natexlab{}.
\newblock \showarticletitle{How Much Water Does Turf Removal Save? Applying
  Bayesian Structural Time-Series to California Residential Water Demand}.
\newblock  (\bibinfo{year}{[n.\,d.]}).
\newblock


\bibitem[\protect\citeauthoryear{Wang, Li, and Li}{Wang et~al\mbox{.}}{2018}]%
        {WANG2018821}
\bibfield{author}{\bibinfo{person}{Qiang Wang}, \bibinfo{person}{Shuyu Li},
  {and} \bibinfo{person}{Rongrong Li}.} \bibinfo{year}{2018}\natexlab{}.
\newblock \showarticletitle{Forecasting energy demand in China and India: Using
  single-linear, hybrid-linear, and non-linear time series forecast
  techniques}.
\newblock \bibinfo{journal}{\emph{Energy}}  \bibinfo{volume}{161}
  (\bibinfo{year}{2018}), \bibinfo{pages}{821 -- 831}.
\newblock
\showISSN{0360-5442}
\urldef\tempurl%
\url{https://doi.org/10.1016/j.energy.2018.07.168}
\showDOI{\tempurl}


\bibitem[\protect\citeauthoryear{Wytock and Kolter}{Wytock and Kolter}{2013}]%
        {wytock2013sparse}
\bibfield{author}{\bibinfo{person}{Matt Wytock} {and} \bibinfo{person}{Zico
  Kolter}.} \bibinfo{year}{2013}\natexlab{}.
\newblock \showarticletitle{Sparse Gaussian conditional random fields:
  Algorithms, theory, and application to energy forecasting}. In
  \bibinfo{booktitle}{\emph{International conference on machine learning}}.
  \bibinfo{pages}{1265--1273}.
\newblock


\bibitem[\protect\citeauthoryear{Yabe, Tsubouchi, and Sekimoto}{Yabe
  et~al\mbox{.}}{2017}]%
        {yabe2017cityflowfragility}
\bibfield{author}{\bibinfo{person}{Takahiro Yabe}, \bibinfo{person}{Kota
  Tsubouchi}, {and} \bibinfo{person}{Yoshihide Sekimoto}.}
  \bibinfo{year}{2017}\natexlab{}.
\newblock \showarticletitle{CityFlowFragility: Measuring the Fragility of
  People Flow in Cities to Disasters using GPS Data Collected from
  Smartphones}.
\newblock \bibinfo{journal}{\emph{Proceedings of the ACM on Interactive,
  Mobile, Wearable and Ubiquitous Technologies}} \bibinfo{volume}{1},
  \bibinfo{number}{3} (\bibinfo{year}{2017}), \bibinfo{pages}{117}.
\newblock


\bibitem[\protect\citeauthoryear{Zhang and Lam}{Zhang and Lam}{2018}]%
        {zhang2018practical}
\bibfield{author}{\bibinfo{person}{Zhiang Zhang} {and}
  \bibinfo{person}{Khee~Poh Lam}.} \bibinfo{year}{2018}\natexlab{}.
\newblock \showarticletitle{Practical implementation and evaluation of deep
  reinforcement learning control for a radiant heating system}. In
  \bibinfo{booktitle}{\emph{Proceedings of the 5th Conference on Systems for
  Built Environments}}. ACM, \bibinfo{pages}{148--157}.
\newblock


\end{thebibliography}
